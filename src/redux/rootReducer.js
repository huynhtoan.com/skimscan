import { combineReducers } from 'redux';

import questionReducer from './question/questionReducer';

const rootReducer = combineReducers({
  question: questionReducer
});

export default rootReducer;
