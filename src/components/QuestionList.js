import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Container from "@material-ui/core/Container";
import AdvancedTable from "./AdvancedTable";
import axios from "axios";
import questionConfig from "../config/question";
import api from '../config/api';

class QuestionList extends Component {
  state = {
    questions: [],
    searchString: "",
  };

  constructor() {
    super();
    // this.getQuestions();
  }

  getQuestions = () => {
    axios
      .get(
        [
          api.endpoint,
          api.questionList,
          api.query,
          this.state.searchString,
        ].join("")
      )
      .then((response) => {
        this.setState({ questions: response.data });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  onSearchInputChange = (event) => {
    if (event.target.value) {
      this.setState({ searchString: event.target.value });
    } else {
      this.setState({ searchString: "" });
    }
    setTimeout(() => {
      this.getQuestions();
    }, 0);
  };

  render() {
    return (
      <div className="sidebar">
        {this.state.questions ? (
          <Container>
            <TextField
              id="searchInput"
              placeholder="Search for question by ID or any text"
              onChange={this.onSearchInputChange}
              fullWidth
            />
            <AdvancedTable
              headCells={questionConfig.headCells}
              rows={this.state.questions}
              pathPrefix={questionConfig.pathPrefix}
            />
          </Container>
        ) : (
          "No questions found"
        )}
      </div>
    );
  }
}

export default QuestionList;
