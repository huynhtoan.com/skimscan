import axios from "axios";
import questionConfig from "../../config/question";
import api from "../../config/api";
import {
  FETCH_QUESTIONS_REQUEST,
  FETCH_QUESTIONS_SUCCESS,
  FETCH_QUESTIONS_FAILURE,
} from "./questionTypes";

import { fetchQuestionsSuccess } from "./questionActions";
import { useDispatch, useSelector } from "react-redux";

const initialState = {
  loading: false,
  questions : [],
  error: ''
}

const questionReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_QUESTIONS_REQUEST:
      return {
        ...state,
        loading: true
      }
    case FETCH_QUESTIONS_SUCCESS:
      return {
        loading: false,
        questions: action.payload,
        error: ''
      }
    case FETCH_QUESTIONS_FAILURE:
      return {
        loading: false,
        questions: [],
        error: action.payload
      }
    default:
      return state
  }
}

export const loadQuestions = () => async(dispatch, getState) => {
  const questions = await axios
    .get([api.endpoint, api.questionList, api.query].join(""))
    .then((response) => response.data)
    .catch((error) => {
      console.log(error);
    });

  dispatch(fetchQuestionsSuccess(questions));
}

export default questionReducer;