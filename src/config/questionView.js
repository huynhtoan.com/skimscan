let question = {
  headCells: [
    {
      id: "answer",
      numeric: false,
      disablePadding: false,
      label: "Answer"
    }
  ],
  saveButtonText: "Save Question",
  savedButtonText: "Saved!"
};

export default question;
