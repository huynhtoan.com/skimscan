describe("The Home Page", function() {
  it("should successfully loads", function() {
    cy.visit("/");
  });

  it("should contains search form", function() {
    cy.get("#searchInput").should("be.visible");
  });

  it("should contains question lists", function () {
    cy.get("tbody").find("tr").its("length").should("be.eq", 20);
  });

  it("should performs search successfully", function() {
    cy.get("#searchInput")
      .focus()
      .type("S1001");
    cy.get("tbody")
      .find("tr")
      .its("length")
      .should("be.greaterThan", 0);
  });

  it("question link should clickable then go to the detail page", function () {
    cy.visit("/");
    cy.get(".to-detail-page").first().click();
    cy.get("h1").should("contain.text", "Question");
  });

  it("detail page should contains required elements", function() {
    cy.get("h1").should("be.visible");
    cy.get(".detail-card").should("be.visible");
  });

  it("save function should works", function() {
    let textCollection = ["foo", "bar", "foobar", "barfoo"];
    let randomNumber = parseInt(Math.random() * textCollection.length);
    let randomText = textCollection[randomNumber];
    cy.get("#question_name").focus().clear().type(randomText);
    cy.get(".save_question").click();
    cy.reload();
    cy.get("#question_name").should("contain.value", randomText);
  });
});
