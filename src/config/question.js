let question = {
  headCells: [
    { id: "id", numeric: false, disablePadding: false, label: "ID" },
    {
      id: "name",
      numeric: false,
      link: true,
      disablePadding: true,
      label: "Name"
    }
  ],
  pathPrefix: "q"
};

export default question;
