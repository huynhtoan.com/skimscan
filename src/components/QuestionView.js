import React, { Component } from "react";
import Container from "@material-ui/core/Container";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Chip from "@material-ui/core/Chip";
import axios from "axios";
import api from "../config/api";
import BasicTable from "./BasicTable";
import questionViewConfig from "../config/questionView";

class QuestionView extends Component {
  state = {
    question: {},
    questionName: "",
    buttonText: "Save Question",
  };
  constructor({  }) {
    super();
  }

  onQuestionNameChange = (event) => {
    this.setState({ questionName: event.target.value });
    this.setState({ buttonText: questionViewConfig.saveButtonText });
  };

  saveName = () => {
    let newQuestion = this.state.question;
    let questionId = newQuestion.id;
    newQuestion.name = this.state.questionName;
    axios
      .put(api.endpoint + api.questionView + questionId, newQuestion)
      .then((response) => {
        this.setState({ question: newQuestion });
        this.setState({ buttonText: questionViewConfig.savedButtonText });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  render() {
    return (
      <div className="main-view">
        {this.state.question ? (
          <Container className="">
            <h1>
              Question: #{this.state.question.id}
              <Chip
                className={`label-${this.state.question.status}`}
                label={this.state.question.status}
              />
            </h1>
            <Button
              className="save_question"
              variant="contained"
              color="primary"
              onClick={this.saveName}
            >
              {`${this.state.buttonText}`}
            </Button>
            <Card className="detail-card">
              <CardContent>
                <Typography color="textSecondary" gutterBottom>
                  <i>#{this.state.question.id}</i>
                </Typography>
                <Typography color="textSecondary" gutterBottom>
                  Name
                </Typography>
                <TextField
                  id="question_name"
                  name="question_name"
                  fullWidth
                  value={`${this.state.questionName}`}
                  onChange={this.onQuestionNameChange}
                />
                <TextField
                  id="question_answer"
                  name="question_answer"
                  fullWidth
                  value={`${this.state.question.answer}`}
                />
              </CardContent>
            </Card>
          </Container>
        ) : (
          "No question found for ID"
        )}
      </div>
    );
  }
}

export default QuestionView;
