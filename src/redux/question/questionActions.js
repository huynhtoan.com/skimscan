import {
  FETCH_QUESTIONS_REQUEST,
  FETCH_QUESTIONS_SUCCESS,
  FETCH_QUESTIONS_FAILURE,
} from "./questionTypes";

export const fetchQuestionsRequest = () => {
  return {
    type: FETCH_QUESTIONS_REQUEST
  }
}

export const fetchQuestionsSuccess = questions => {
  return {
    type: FETCH_QUESTIONS_SUCCESS,
    payload: questions,
  };
};

export const fetchQuestionsFailure = error => {
  return {
    type: FETCH_QUESTIONS_FAILURE,
    payload: error
  };
};