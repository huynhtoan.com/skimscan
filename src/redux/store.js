import rootReducer from "./rootReducer";
import { loadQuestions } from "./question/questionReducer";
import { createStore, compose, applyMiddleware } from "redux";
import thunk from 'redux-thunk';

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancer(applyMiddleware(thunk)));

store.dispatch(loadQuestions());

export default store;
