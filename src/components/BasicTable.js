import React from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

export default function BasicTable(props) {
  const { headCells, rows } = props;
  return (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow key="tablehead">
            {headCells.map((headCell, key) => {
              return (
                <TableCell key={key} align="left">
                  <strong>{headCell.label}</strong>
                </TableCell>
              );
            })}
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row, key) => {
            return (
              <TableRow key={key}>
                {headCells.map((headCell, key) => {
                  return (
                    <TableCell key={key} align="left">
                      {row[headCell.id]}
                    </TableCell>
                  );
                })}
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
