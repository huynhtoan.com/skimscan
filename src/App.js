import React from 'react';
import AppBar from './components/Navbar';
import LeftBar from "./components/LeftBar";
import RightBar from "./components/RightBar";
import QuestionList from "./components/QuestionList";
import QuestionView from "./components/QuestionView";
import './App.css';
import { BrowserRouter, Route } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <AppBar></AppBar>
        <LeftBar></LeftBar>
        <QuestionView></QuestionView>
        <QuestionList></QuestionList>
      </div>
    </BrowserRouter>
  );
}

export default App;
