import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import {Link} from 'react-router-dom';

const NavbBar = () => {
  return (
    <AppBar position="static">
      <Container>
        <Toolbar>
          <Typography variant="inherit" color="inherit">
            <h2>
              <Link to="/" className="white">
                Questions
              </Link>
            </h2>
          </Typography>
        </Toolbar>
      </Container>
    </AppBar>
  );
}

export default NavbBar;
